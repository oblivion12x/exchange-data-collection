# exchange-data-collection
### Prerequisites

To run this application recent Node.js version and RabbitMQ server should be installed on the system. After that follow below in cmd


* Clone this repository:

      * git clone https://oblivion12x@bitbucket.org/oblivion12x/exchange-data-collection.git
      * cd exchange-data-collection
	  * npm install
	  * npm start

* To Receive messages in  RabbitMQ ticker queue:
      * cd exchange-data-collection\exchange-data-collection\src\broker
      * receive node.js