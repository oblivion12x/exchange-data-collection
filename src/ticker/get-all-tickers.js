// fetches ticker data from the exchange
import * as _ from "lodash";
var moment = require("moment");
import {  FileName } from "../common/common-functions";
var https = require('https');
const fileClass = new FileName();
fileClass.updateFile("./rawdata/orderbook/");
export default class DownloadTickers  {
     getTickers(exchange, id= "") {
        switch (exchange) {   
            case "kraken": {
                return new Promise((resolve, reject) => {
                    if (id !== "") {
                        // console.log(id)
                        setTimeout(requestData , 1000 )
                        function requestData(){
                            const urlkraken= `https://api.kraken.com/0/public/Ticker?pair=${id}`;
                            https.get(urlkraken, function(res){
                                let body = '';
                                res.on('data', function(chunk){
                                    body += chunk;
                                });
                                res.on('end', function(){
                                    if (!Object.keys(body).length) {
                                        console.log('empty resolve')
                                        resolve([]); }
                                    try{
                                        const  parsed= JSON.parse(body).result
                                    resolve(parsed);
                                    }catch(err){
                                        reject(err)
                                    }
                                });
                            }).on('error', function(e){
                                reject(e);
                                  console.log("Got an error: ", e);
                            });
                        }
                    } else {
                        reject("Empty id kraken");
                    }
                });
            }       
            default: {
                return new Promise((resolve, reject) => {
                    resolve({ message: "No Exchange Found" });
                });
            }
        }
    } 
     convertToISOString(dateInput) {
        return moment.unix(dateInput).toISOString();
    }
}
