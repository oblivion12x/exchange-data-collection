
import * as async from "async";
import * as _ from "lodash";
import { exchanges} from "../exchanges/constants";
import {constantPairs} from '../common/common-constants'
import GetrawPairs from "../market/get-downloaded-pairs";
import DownloadTickers from "./get-all-tickers"
import { sendToQueue } from "../broker/send";
const DownloadTickerObject = new DownloadTickers();
const DownloadRawObject = new GetrawPairs();
let pairsId = "";
let returnCurrency
var newconstantPairs = constantPairs.map((item)=>item.replace('/',''));
var moment = require("moment");
String.prototype.replaceAt3=function(char) {
    var a = this.split("");
    a[0] = char;
    a[4] = char
    return a.join("");
}    
String.prototype.replaceAt4=function(char) {
    var a = this.split("");
    a[4] = char
    return a.join("");
} 
export function updateTickers() { 
    async.eachSeries(exchanges, async (exchangeName, outerCallback) => { // fetches exchange name 
         returnCurrency = await DownloadRawObject.getallCurrencyPairs(exchangeName); // fetches exchange pairs from saved file
           await reduceData(returnCurrency, exchangeName, ()=>{ 
            outerCallback();
           });
    }, async (err) => {
        updateTickers()
    });
}
const reduceData = (currency, exchangeName, outerCallback2) => { // forward the pairs to getTicker module and forward the data to 'transform data ' function
    async.retry({times: 3, interval: 10000}, (back) => {
            pairsId =  _.intersection( newconstantPairs , currency ).join()
            DownloadTickerObject.getTickers(exchangeName, pairsId).then(async (result) => {
                if(exchangeName=='kraken'){
                let splitAt = index => x => [x.slice(0, index), x.slice(index)]
                for (var key in result) {
                    if (result.hasOwnProperty(key)) {
                        let splitCurrency
                        let symbol
                        if(key.length ==8 &&  key.charAt(0)=='X' && key.charAt(4) ==='Z' ){                   
                             symbol = key.replaceAt3("");
                             splitCurrency = splitAt(3)(symbol)
                            transformData(result[key] , splitCurrency , exchangeName)
                        }else if (key.length ==6){
                            splitCurrency = splitAt(3)(key)
                            transformData(result[key] , splitCurrency , exchangeName)
                        }else if (key.length ==8 && key.charAt(0)=='X' && key.charAt(4)==='X' ){
                             symbol = key.replaceAt3("");
                            splitCurrency = splitAt(3)(symbol)
                            transformData(result[key] , splitCurrency , exchangeName)
                        }else if (key.length ==8 && key.charAt(4)=='Z' ){
                            symbol = key.replaceAt4("");
                            splitCurrency = splitAt(4)(symbol)
                            transformData(result[key] , splitCurrency , exchangeName)
                        }
                    }
                 }back()
                }else{
                   back(null, null) 
                }
            }).catch(async (err) => {
                    console.log("Error  Retrying in 10 sec , ", err, exchangeName);
                    sendToQueue([{err:err.name}])
                    back(err);
            });    
    },  () => {
        outerCallback2();
    });
};

function transformData(result , splitCurrency , exchange){ // transforms the data and forward to be sent via queue
    let tickerData=[]
    tickerData.push({
        ask: result.a[0],
        ask_size:result.a[1],
        bid:result.b[0],
        bid:result.b[1],
        exchange:exchange,
        high:result.h[0],
        last_price:result.c[0],
        symbol:splitCurrency[0].toLowerCase()+'_'+splitCurrency[1].toLowerCase(),
        timestamp:moment().utc().toISOString(),
        volume:result.v[0],     
     })
     sendToQueue(tickerData)
}

