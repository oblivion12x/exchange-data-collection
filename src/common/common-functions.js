import * as fs from "fs";
var mkdirp = require('mkdirp');
function saveData(file, formatedData) { // write data to the file 
    return new Promise((resolve, reject) => {
         const createPath = file.substring(0, file.lastIndexOf("/") + 1);
         mkdirp(createPath, (err) => {
             if (err) {
                 console.log("Error while creating path", err);
             }
             fs.writeFile(file, formatedData , "utf8", (error) => {
                 if (error) {
                     console.log("Error in file save", error);
                     resolve(false);
                 }
                 resolve(true);
             });
         });
     });
 }

  // return and update filename
  class FileName {
    file= "./public";
   getfile() {
       return this.file;
   }
   updateFile(newName) {
       this.file = newName;
   }
}
// walk thru all the files and return the sync all file path
const walkSync = (dir, filelist) => {
    const allfiles = fs.readdirSync(dir);
    filelist = filelist || [];
    allfiles.forEach((file) => {
        if (fs.statSync(dir + "/" + file).isDirectory()) {
            filelist = walkSync(dir + "/" + file, filelist);
        } else {
            filelist.push(file);
        }
    });
    return filelist;
};

export {FileName , saveData , walkSync}