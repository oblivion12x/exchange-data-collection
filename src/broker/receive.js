// To receive the message in Rabbit MQ ticker
const amqp = require('amqplib/callback_api');
amqp.connect('amqp://localhost', function(err, conn) {
    conn.createChannel(function(err, ch) {
      const q = 'ticker';
      ch.assertQueue(q, { durable: true });
      
      console.log(" [*] Waiting for ticker in %s. To exit press CTRL+C", q);
      ch.consume(q, async function(ticker) {
        // console.log(" [x] Received %s", ticker.content.toString());
        let tickerData = JSON.parse(ticker.content.toString());
console.log(tickerData)
        ch.ack(ticker);
      }, { noAck: false });
    });
  });
