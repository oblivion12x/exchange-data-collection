// send the ticker to rabbit MQ Queue
export function sendToQueue(ticker) {
require('amqplib/callback_api')
  .connect('amqp://localhost', function(err, conn) {
    if (err != null) bail(err);
    publisher(conn , ticker);
  });
}
var q = 'ticker';
 
function bail(err) {
  console.error(err);
  process.exit(1);
}
// Publisher
function publisher(conn , data) {
  conn.createChannel(on_open);
  function on_open(err, ch) {
    if (err != null) bail(err);
    ch.assertQueue(q);
    ch.sendToQueue(q, Buffer.from(JSON.stringify(data)));
    // console.log("Ticker sent to queue : ", data);
    setTimeout(function() { conn.close()}, 500);
  }
}

