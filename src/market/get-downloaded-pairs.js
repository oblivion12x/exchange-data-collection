import * as fs from "fs";
import * as _ from "lodash";
import {FileName, walkSync} from "../common/common-functions";
// fetches raw data from saved file and forward the pairs to fetch ticker data
export default class GetrawPairs { 
     fileClass;
    constructor() {
        this.fileClass = new FileName();
        this.fileClass.updateFile("./raw_pairs/");
    }
     getallCurrencyPairs(exchageName) {
        return new Promise((resolve, reject) => {
            const files = [];
            let fileJson = "";
            const filelist = walkSync("./raw_pairs/", files);
            if (filelist.indexOf(exchageName + ".json") !== -1) {
                fileJson = fs.readFileSync(this.fileClass.getfile() + exchageName + ".json", { encoding: "utf8" });
            }
            this.getPairs(exchageName, fileJson).then((result) => {
                resolve(result);
            });
        });
    }
      getPairs(exchange, pairs) {
            switch (exchange) {
            case "kraken":
            return new Promise((resolve, reject) => {
                pairs = JSON.parse(pairs).result;
                let assets = [];
                _.map(pairs, (market, key) => {
                    assets.push(market.altname.replace('.d',''));
                });
                assets = _.uniq(assets);
                resolve(assets);
            }).catch(function(err){
                console.log(err)
            })
            default:
            return new Promise((resolve, reject) => {
                resolve([]);
            });
        }
    }
}
