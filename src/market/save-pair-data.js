
// generate file name for raw pair data and forward the data to be saved.
import * as async from "async";
import * as _ from "lodash";
import {exchanges} from '../exchanges/constants';
import {FileName , saveData} from '../common/common-functions'
import {getallCurrencyPairs} from './get-exchange-pairs'
const fileClass = new FileName();
fileClass.updateFile("./raw_pairs/");
 export function updateRawPairs() {
    async.parallel({
        rawDatafunction: (callback) => {
            Promise.all(exchanges).then((values) => {
                async.forEachOf(exchanges, (exchange, key, done) => {
                    getallCurrencyPairs(exchange).then((data) => {
                        if (data.length || Object.keys(data).length) {
                            let extension = "json";
                            saveData(fileClass.getfile() + exchange + "." + extension, JSON.stringify(data)).then((returned) => {
                                if (returned) {
                                    console.log("Success Please check the file =>" + fileClass.getfile() + exchange);
                                }
                            });
                        }
                        done();
                    }).catch((err) => {
                        return console.log(err, "error on" + exchange);
                    });
                }, (error) => {
                    if (error) { console.log(error); }
                    callback(); });
            });
        },
    }, (err, result) => {
        if (err) { console.log(err); }
    });
}