// fetches pair data from exchange
var https = require('https');
import * as _ from "lodash";
export function getallCurrencyPairs(exchange){
    switch(exchange){
        case 'kraken':
        return new Promise((resolve, reject) => {
        https.get('https://api.kraken.com/0/public/AssetPairs', function(res){
            let body = '';
            let obj ={}
            res.on('data', function(chunk){
                body += chunk;
            });
            res.on('end', function(){
                const exchangeResponse = JSON.parse(body);
                resolve(exchangeResponse);
            });
        }).on('error', function(e){
            resolve([]);
              console.log("Got an error: ", e);
        });
    });  
        }
    }
