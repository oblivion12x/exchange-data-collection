import express from 'express';
import bodyParser from 'body-parser';
import path from 'path';
import {updateRawPairs} from './src/market/save-pair-data'
import {updateTickers} from './src/ticker/update-tickers'
const app         = express();
app.use(express.static(path.join(__dirname, 'src/public'))); 
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
    app.listen(3004, function(err) {
      // to fetch pairs from exchange
        // updateRawPairs() 
      // to fetch ticker data and provide it to message broker  
        updateTickers()
      console.log('fetching ticker data')
      console.log('Listening on port 3004...')
    })



